# screeps_scripts

## Dependencies.
* [screeps-typescript-starter](https://screepers.gitbook.io/screeps-typescript-starter/)

## Use sideload.ps1 to rollout ts->js and import to screeps.

1) Clone each repository in the same directory then you can use sideload to push to the game as described below.

```
.\sideload.ps1
```

* Modify the variables in sideload.ps1 as needed.
* You can modify the desired branch, server IP & Port.
* Only modify `$Appdata` if you know what you're doing.
* Only modify `$sleepTime` if you find the game misses a code update.
