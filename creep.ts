export class creep {
    static working = 0;
    static debug = function(creep: Creep){
        console.log("Creep Free Capacity: " + creep.store.getFreeCapacity());
        console.log("Creep Used Capacity: " + creep.store.getUsedCapacity());
        console.log("Creep Total Capacity: " + creep.store.getCapacity());
        const percent = creep.store.getUsedCapacity() * 100 / creep.store.getCapacity();
        console.log("Creep Percentage Capacity: " + percent + "%" );
    }
    static task = class {
        static determine = function(sCreep: Creep){
            if (sCreep.memory.task != 'Gathering' && creep.store.isEmpty(sCreep)){
                if (sCreep.memory.task = 'building'){creep.working--;}
                sCreep.memory.task = 'Gathering';
                sCreep.say(sCreep.memory.task + '!');
                //console.log(sCreep.name + "'s " + sCreep.memory.task + " task set.");
            }
            if(creep.store.isFull(sCreep) && ((Game.spawns['Spawn1'].store.getUsedCapacity(RESOURCE_ENERGY)*100) / Game.spawns['Spawn1'].store.getCapacity(RESOURCE_ENERGY) ) >= 80 ) {
                
                var sites = sCreep.room.find(FIND_CONSTRUCTION_SITES).length;
                console.log("construction sites: " + sites);
                console.log("creep.filter.task(sCreep, 'Upgrading').length: " + creep.filter.task(sCreep, 'Upgrading').length);
                if(sCreep.memory.task != 'Building' && creep.filter.task(sCreep, 'Upgrading').length > 1 && sCreep.room.find(FIND_CONSTRUCTION_SITES).length > 0) {
                    sCreep.memory.task = 'Building';
                    sCreep.memory.target = creep.working;
                    creep.working++;
                    sCreep.say(sCreep.memory.task+ '!');
                    //console.log(sCreep.name + "'s " + sCreep.memory.task + " task set.");
                }
                if(sCreep.memory.task != 'Upgrading' && !sCreep.room.find(FIND_CONSTRUCTION_SITES).length) 
                {
                    if (sCreep.memory.task = 'building'){creep.working--;}
                    sCreep.memory.task = 'Upgrading';
                    sCreep.say(sCreep.memory.task + '!');
                    console.log(sCreep.name + "'s " + sCreep.memory.task + " task set.");
                }
            }
        }
        static run = function(sCreep: Creep){
            if(sCreep.memory.task == 'Gathering'){
                creep.task.perform.gathering(sCreep);
            }
            if(sCreep.memory.task == 'Upgrading'){
                creep.task.perform.upgrading(sCreep);
            }
            if(sCreep.memory.task == 'Building'){
                creep.task.perform.building(sCreep);
            }
        }
        static perform = class{
            static gathering = function(creep: Creep){
                if (creep.store.getFreeCapacity() > 0 ){
                    var sources = creep.room.find(FIND_SOURCES);
                    if (creep.harvest(sources[0]) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(sources[0]), {visualizePathStyle: {stroke: '#ffaa00'}}; 
                    }
                }
                else {
                    if(Game.spawns['Spawn1'].store.getFreeCapacity(RESOURCE_ENERGY) >= (Game.spawns['Spawn1'].store.getCapacity(RESOURCE_ENERGY) / (3 * 2) ) ) {
                        if(creep.transfer(Game.spawns['Spawn1'], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE){
                            creep.moveTo(Game.spawns['Spawn1']);
                        }
                        else {
                            creep.say("<3 friends");
                        }
                    }
                }
            }
            static upgrading = function(creep: Creep){
                if (creep.room.controller != null)
                {
                    if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(creep.room.controller), {visualizePathStyle: {stroke: '#bff5ab'}};      
                    }
                }
            }
            static building = function(creep: Creep){

                var targets = creep.room.find(FIND_CONSTRUCTION_SITES);

                if(targets.length) {
                    //console.log('pre-for loop');
                    var err = creep.build(targets[creep.memory.target]);
                    console.log(creep.name + "construction target: " + creep.memory.target);
                    //console.log(creep.name + ' checks targets['+creep.memory.target+'].progress of ' + targets.length + ' : ' + targets[creep.memory.target].progress);
                    //console.log("err: " + err);
                    if(err == ERR_NOT_IN_RANGE ) {
                        creep.moveTo(targets[creep.memory.target], {visualizePathStyle: {stroke: '#ffffff'}});
                        console.log(creep.name + " moved toward task.");
                    }
                    if(err == OK) { 
                        console.log(creep.name + "  performed task.");
                    }
                }
            }
        }
    }
    static filter = class {
        static role = function(creep: Creep){}
        static task = function(sCreep: Creep, Task: string){
            console.log("task: "+ Task);
            return _.filter(Game.creeps, (sCreep) => sCreep.memory.task = Task);
        }
    }
    static store = class {
        static isEmpty = function(creep: Creep){
            if( creep.store.getCapacity() == creep.store.getFreeCapacity() ){
                return true;
            }
            else return false;
        }
        static isFull = function(creep: Creep){
            if( creep.store.getCapacity() == creep.store.getUsedCapacity() ){
                return true;
            }
            else return false;
        }
    }
    static part = class {
        static cost = function(value: BodyPartConstant, index: any, array: any) {
            return BODYPART_COST[value];
        }
        static sum = function(total: any, value: any, index: any, array: any){
            return total + value;
        }
    }
    static spawn = class {
        static new = function(Spawn: string, Role: string, bodyparts: BodyPartConstant[]){
            var newName = Role + Game.time;
            console.log("Spawning new " + Role + ": " + newName);
            Game.spawns[Spawn].spawnCreep(bodyparts, newName, {memory: {role: Role, task : "Gathering", target: 0, working: false}});
        }
    }
}
