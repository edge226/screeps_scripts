var creep = {
    
    /** @param {creep} creep **/
    debug: function(creep) {
        console.log("Creep Free Capacity: " + creep.store.getFreeCapacity());
        console.log("Creep Used Capacity: " + creep.store.getUsedCapacity());
        console.log("Creep Total Capacity: " + creep.store.getCapacity());
        var percent = creep.store.getUsedCapacity() * 100 / creep.store.getCapacity();
        console.log("Creep Percentage Capacity: " + percent + "%" );
    },

    /** @param {creep} creep **/
    /** @description Gather when energy is empty until energy is full. **/
    gathering: function(creep){
        if (creep.store.getFreeCapacity() > 0 ){
            var sources = creep.room.find(FIND_SOURCES);
            if (creep.harvest(sources[0]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(sources[0]), {visualizePathStyle: {stroke: '#ffaa00'}}; 
            }
        }
        else {
            if(Game.spawns['Spawn1'].store.getFreeCapacity(RESOURCE_ENERGY) >= (Game.spawns['Spawn1'].store.getCapacity(RESOURCE_ENERGY) / (3 * 2) ) ) {
                if(creep.transfer(Game.spawns['Spawn1'], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE){
                    creep.moveTo(Game.spawns['Spawn1']);
                }
                else {
                    creep.say("<3 friends");
                }
            }
        }
    },
    
    /** @param {creep} creep **/
    /** @description upgrade when worker energy is full until empty. **/
    upgrading: function(creep){
        if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller), {visualizePathStyle: {stroke: '#bff5ab'}};      
        }
    },
    
    /** @param {creep} creep **/
    /** @description build when worker energy is full until empty. **/
    building: function(creep){
        var targets = creep.room.find(FIND_CONSTRUCTION_SITES);

        if(targets.length) {
            //console.log('pre-for loop');
            var err = creep.build(targets[creep.memory.target]);
            console.log(creep.name + "construction target: " + creep.memory.target);
            //console.log(creep.name + ' checks targets['+creep.memory.target+'].progress of ' + targets.length + ' : ' + targets[creep.memory.target].progress);
            //console.log("err: " + err);
            if(err == ERR_NOT_IN_RANGE && err != ERR_BUSY ) {
                creep.moveTo(targets[creep.memory.target], {visualizePathStyle: {stroke: '#ffffff'}});
                console.log(creep.name + " moved toward task.");
            }
            if(err == OK) { 
                console.log(creep.name + "  performed task.");
            }
        }
            //console.log('out of for loop.');         
    },
    
    /** @param {creep} creep **/
    /** @description check whether worker energy is empty. **/
    isEmpty: function(creep){
        if( creep.store.getCapacity() == creep.store.getFreeCapacity() ){
            return true;
        }
        else return false;
    },
    
    /** @param {creep} creep **/
    /** @description check whether worker energy is full. **/
    isFull: function(creep){
        if( creep.store.getCapacity() == creep.store.getUsedCapacity() ){
            return true;
        }
        else return false;
    },
    partcost: function(value, index, array){
        return BODYPART_COST[value];
    },
}

module.exports = creep;
