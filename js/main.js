var creep = require('creep');
var math = require('math');
var spawn = require('spawn');
var task = require('task');

module.exports.loop = function() {
    
    var workers = _.filter(Game.creeps, (creep) => creep.memory.role = 'Worker');
    console.log('Workers: ' + workers.length);
    var maxworkers = 3;

    // testing arrays and summing the parts so I can do neat things later.
    const bodyparts = [WORK,CARRY,MOVE];

    if(workers.length < maxworkers // Limit to the max # of workers as defined.
        && Game.spawns['Spawn1'].store.getUsedCapacity(RESOURCE_ENERGY) // Make sure we have the energy before we try to spawn something.
        >= bodyparts.map(creep.partcost).reduce(math.sum)){
            spawn.creep("Spawn1", "Worker", bodyparts);
    }
    
    for(var name in Game.creeps) {
        var sCreep = Game.creeps[name]; // sCreep == selected creep.
        
        // creep.debug(sCreep);
        console.log(Memory.creeps[sCreep.name].task);
        task.determine(sCreep);
        task.perform(sCreep);
    }
}
