var math = {
    inc: function(num){
        return num+1;
    },
    dec: function(num){
        return num-1;
    },
    sum: function(total, value, index, array){
        return total + value;
    }
}

module.exports = math;
