var spawn = {
    determine: function(){

    },
    creep: function(Spawn, Role, bodyparts){
        var newName = Role + Game.time;
        console.log("Spawning new " + Role + ": " + newName);
        Game.spawns[Spawn].spawnCreep(bodyparts, newName, {memory: {role: Role}});
    },
}

module.exports = spawn;
