var creep = require('creep');
var working = 0;
var task = {
    determine: function(sCreep){
        if (sCreep.memory.task != 'Gathering' && creep.isEmpty(sCreep)){
            if (sCreep.memory.task = 'building'){working--;}
            sCreep.memory.task = 'Gathering';
            sCreep.memory.target = null;
            sCreep.say(sCreep.memory.task + '!');
            //console.log(sCreep.name + "'s " + sCreep.memory.task + " task set.");
        }
        if(creep.isFull(sCreep) && ((Game.spawns['Spawn1'].store.getUsedCapacity(RESOURCE_ENERGY)*100) / Game.spawns['Spawn1'].store.getCapacity(RESOURCE_ENERGY) ) >= 80 ) {
            
            var sites = sCreep.room.find(FIND_CONSTRUCTION_SITES).length;
            console.log("construction sites: " + sites);
            console.log("task.filter(sCreep, 'Upgrading').length: " + task.filter(sCreep, 'Upgrading').length);
            if(sCreep.memory.task != 'Building' && task.filter(sCreep, 'Upgrading').length > 1 && sCreep.room.find(FIND_CONSTRUCTION_SITES).length > 0) {
                sCreep.memory.task = 'Building';
                sCreep.memory.target = working;
                working++;
                sCreep.say(sCreep.memory.task+ '!');
                //console.log(sCreep.name + "'s " + sCreep.memory.task + " task set.");
            }
            if(sCreep.memory.task != 'Upgrading' && !sCreep.room.find(FIND_CONSTRUCTION_SITES).length) 
            {
                if (sCreep.memory.task = 'building'){working--;}
                sCreep.memory.task = 'Upgrading';
                sCreep.memory.target = null;
                sCreep.say(sCreep.memory.task + '!');
                console.log(sCreep.name + "'s " + sCreep.memory.task + " task set.");
            }
        }
        
    },
    perform: function(sCreep){
        if(sCreep.memory.task == 'Gathering'){
            creep.gathering(sCreep);
        }
        if(sCreep.memory.task == 'Upgrading'){
            creep.upgrading(sCreep);
        }
        if(sCreep.memory.task == 'Building'){
            creep.building(sCreep);
        }
    },
    filter: function(sCreep, task){
        console.log("task: "+ task);
        return _.filter(Game.creeps, (sCreep) => sCreep.memory.task = task);
    }
}

module.exports = task;
