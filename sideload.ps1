﻿# This script will sideload screeps-typescript-starter,
# rollout -c on code in this directory,
# remove the existing javascript files and replace with the new rollup bundle.

$branch = "default"
$ip = "127_0_0_1"
$port = "21025"
$server = $ip+"___"+$port
$AppData = "AppData\Local\Screeps\scripts"
$sleepTime = 0.5

Copy-Item .\*.ts ..\screeps-typescript-starter\src\ # Copy all typescript files into the src.
Push-Location ..\screeps-typescript-starter\ # Move into the screeps-typescript-starter location to run the rollup.
rollup -c # Rollup the code, If you want this to upload refer to the screeps-typescript-starter documentation.
Remove-item ~\$AppData\$server\$branch\*.js # Game may not recognize update without removing old code.
Start-Sleep -Seconds $sleepTime # The game files won't recognize the update unless we have a small pause.
Copy-Item .\dist\*.js ~\$AppData\$server\$branch\ # Copy the new javascript to the game location.
Pop-Location # Go back to the original directory.
